// Problem 1: Get board information by boardID using a callback
const fs = require("fs")
const path = require("path")
const pathBoard = path.resolve("../boards.json")

function getBoardInfo(boardID, callback) {
  setTimeout(() => {
    fs.readFile(pathBoard, "utf8", (err, data) => {
      if (err) {
        console.error("Error:", err)
        return
      } else {
        const boards = JSON.parse(data)

        const board = boards.filter((board) => {
          return board.id === boardID
        })
        callback(null,board)
      }
      
    })
  }, 2000)
}

module.exports = getBoardInfo
