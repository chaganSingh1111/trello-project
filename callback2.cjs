// Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. Then pass control back to the code that called it by using a callback function.

const fs = require("fs")
const path = require("path")

const pathList = path.resolve("../lists.json")

function getListsByBoardID(boardID, callback) {
  setTimeout(() => {
    fs.readFile(pathList, "utf8", (err, data) => {
      if (err) {
        console.error("Error :", err)
        return
      } else {
        const lists = JSON.parse(data)
        const boardLists = lists[boardID]

        callback(null, boardLists)
      }
    })
  }, 2000)
}

module.exports = getListsByBoardID
