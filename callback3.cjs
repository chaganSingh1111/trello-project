//Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given data in cards.json. Then pass control back to the code that called it by using a callback function.

const fs = require("fs")
const path = require("path")

const pathCard = path.resolve("../cards.json")

function getCardsByListID(listID, callback) {
    
    setTimeout(() => {

      fs.readFile(pathCard, 'utf8', (err, data) => {

        if (err) {
          console.error('Error :', err)
          return
        }
        else{
            const cards = JSON.parse(data)
            const cardsInList = cards[listID]
            callback(null,cardsInList)
        }
        
      })
    }, 2000)
  }

module.exports = getCardsByListID