// Problem 4: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

//     Get information from the Thanos boards
//     Get all the lists for the Thanos board
//     Get all cards for the Mind list simultaneously


const getBoardInfo = require('./callback1.cjs')
const getListsByBoardID = require('./callback2.cjs')
const getCardsByListID = require('./callback3.cjs')


  
  function getInfoForThanos(boardId){
  
     setTimeout(()=>{
  
        getBoardInfo(boardId,(err, data)=>{
            
          if(err){
              console.error(err)
          }
          else{
              console.log(data)
          }
  
          getListsByBoardID(boardId, (err, data)=>{
              if(err){

                  console.error(err)
              }else{

                 console.log(data)
                 const listId = data.filter((findName)=>{
                  return findName.name === 'Mind'
                })
  
                getCardsByListID(listId[0].id, (err, data)=>{
                      if(err){
                          console.error(err)
                      }else{
                          console.log(data)
                      }
                  })
              }
          })
      })
  
    },2000)
  
  }
  
module.exports = getInfoForThanos